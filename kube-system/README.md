# Kube-system namespace

Namespace containing all the specific system items.

## Descheduler

Removes pods that are breaking affinity rules.

## Intel-gpu-plugin

Intel device plugin exposing GPU resources for use by Plex.

## K8dash

Kubernetes dashboard for pretty cluster visualization.

## Kured

Kubernetes Reboot Daemon to automatically reboot the cluster nodes when needed
for updates.

## MetalLB

Metal load balancer to provide access to the cluster through the network

## NFS client provisioner

Provision storage on NFS volumes.

## NFS-PV

Persistent volumes on NFS.

## Nginx

Ingress controller.

## Sealed Secrets

Seal away secrets to put them in Git without others reading them.
