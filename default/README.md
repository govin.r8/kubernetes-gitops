# Default namespace

## Bazarr

Companion for Sonarr and Radarr that downloads subtitles automatically.

## Bookstack

A nice wiki using a system of books and chapters

## External-dns

`external-dns` takes care of linking the local IP addresses to a DNS address.
In this case, [Linode](https://www.linode.com/) serves as a DNS provider.

## Goldilocks

Monitors the resource usage and gives suggestions on resource limits.

## Guacamole

SSH/RDP/VNC web gateway.

## Jackett

Because it's cold without a Jackett (get it?) and to give Sonarr and Radarr a
centralized system to look up things.

## Minio

[Minio](https://min.io/) provides object storage. Very useful for backups and
other object stores.

## Plex

[Plex](https://www.plex.tv/) is a media server to provide a proper library and
player for all the media.

## Qbittorrent

A torrent client for all the Linux ISOs.

## Radarr

Movie library and tracker.

## Sonarr

TV show library and tracker.

## Tautulli

Plex statistics
